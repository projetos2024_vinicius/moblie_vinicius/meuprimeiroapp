import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, Button, TouchableOpacity, ScrollView } from 'react-native';
import { useState } from 'react';

export default function App() {
  const [width, setWidth] = useState(150);
  const [top, setTop] = useState(460);
  

  const aumenta = () => {
    setWidth(width + 50); // Ajuste o valor do aumento conforme necessário
    setTop(top - 20); // Ajuste o valor do deslocamento para cima conforme necessário
  };
  const desce = () => {
    setTop(top + 20)
  }

  return (

    <View style={styles.container}>
    
    
      <Image
        source={require ('./imagens/carlos.jpg')}
        style={{ width: 100, height: 150, borderRadius: 0, top: top, position: 'absolute' }}
      />
      <Text style={styles.text}>Ajude meu amigo a voar</Text>
      <Button onPress={aumenta} title='Sobe' />
      <Button onPress={desce} title='Desce' />
      <View style={styles.countContainer}>
        <Text style={styles.countText}>altura: {top}</Text>
      </View>

      <StatusBar style="auto" />

    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'pink',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  text: {
    fontSize: 20,
  },
  countContainer: {
    marginTop: 10, // Ajuste a margem conforme necessário
  },
  countText: {
    fontSize: 25,
  },
});
